import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useField } from 'formik';

import ShortMessage from '../ShortMessage/ShortMessage';

import './Input.scss';

const Input = (props) => {
	const { label, className, style, name, placeholder, type, onChange, onBlur } =
		props;
	const [field, meta, helpers] = useField(name);
	const inputClass = classNames(className, 'input', `input--style--${style}`);
	const isError = meta.error && meta.touched;

	const handleChange = (e) => {
		field.onChange(e);
		onChange(e);
	};

	return (
		<label className={inputClass}>
			{label && <span className='input__label-title'>{label}</span>}
			<input
				{...field}
				type={type}
				name={name}
				className='input__control'
				placeholder={placeholder}
				onKeyUp={handleChange}
				onBlur={onBlur}
			/>
			{isError && (
				<ShortMessage
					className='input__message'
					type='alert'
					message={meta.error}
				/>
			)}
		</label>
	);
};

Input.propTypes = {
	value: PropTypes.string,
	placeholder: PropTypes.string,
	className: PropTypes.string,
	style: PropTypes.string,
	label: PropTypes.string,
	type: PropTypes.string,
	name: PropTypes.string.isRequired,
	onChange: PropTypes.func,
	onBlur: PropTypes.func,
};

Input.defaultProps = {
	value: '',
	placeholder: '',
	className: '',
	style: 'black-outline',
	label: '',
	type: 'text',
	onChange: () => {},
	onBlur: () => {},
};

export default Input;
