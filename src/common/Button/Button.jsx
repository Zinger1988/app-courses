import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Button.scss';

const Button = (props) => {
	const { type, className, buttonText, onClick, style, size } = props;
	const btnClass = classNames(
		className,
		'btn',
		`btn--style--${style}`,
		`btn--size--${size}`
	);

	return (
		<button type={type} className={btnClass} onClick={onClick}>
			{buttonText}
		</button>
	);
};

Button.propTypes = {
	className: PropTypes.string,
	buttonText: PropTypes.string.isRequired,
	onClick: PropTypes.func,
	style: PropTypes.string,
	type: PropTypes.string,
	size: PropTypes.string,
};

Button.defaultProps = {
	className: '',
	onClick: () => {},
	style: 'outline-black',
	type: 'button',
	size: 'md',
};

export default Button;
