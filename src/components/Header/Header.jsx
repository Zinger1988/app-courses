import React from 'react';
import PropTypes from 'prop-types';

import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';

import './Header.scss';

const Header = (props) => {
	const { className } = props;
	return (
		<header className={`header ${className}`}>
			<div className='container header__container'>
				<Logo className='header__logo' />
				<div className='header__user'>Anonimus user</div>
				<Button className='header__logout-btn' buttonText='Logout' />
			</div>
		</header>
	);
};

Header.propTypes = {
	className: PropTypes.string,
};

Header.defaultProps = {
	className: '',
};

export default Header;
