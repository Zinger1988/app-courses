import { v4 as uuidv4 } from 'uuid';
import { dateGenerator } from './dateGenerator';

class Course {
	constructor({ title, description, duration, authors }) {
		this.id = uuidv4();
		this.title = title;
		this.description = description;
		this.creationDate = dateGenerator();
		this.duration = +duration;
		this.authors = authors;
	}
}

export default Course;
