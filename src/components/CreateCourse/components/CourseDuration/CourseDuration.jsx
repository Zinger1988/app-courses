import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import Input from '../../../../common/Input/Input';

import { pipeDuration } from '../../../../helpers/pipeDuration';
import './CourseDuration.scss';

const CourseDuration = (props) => {
	const { setDuration, duration } = props;
	const [formattedTime, setFormattedTime] = useState('00:00');

	const handleChange = (e) => {
		const value = e.target.value;
		isNaN(value) ? setDuration(0) : setDuration(value);
	};

	useEffect(() => {
		setFormattedTime(pipeDuration(duration));
	}, [duration]);

	return (
		<div className='course-duration'>
			<Input
				className='course-duration__input'
				style='black-outline'
				label='Duration (minutes)'
				placeholder='Enter course duration...'
				name='duration'
				onChange={handleChange}
			/>
			<div className='course-duration__value'>{formattedTime}</div>
		</div>
	);
};

CourseDuration.propTypes = {
	setDuration: PropTypes.func.isRequired,
	duration: PropTypes.string.isRequired,
};

export default CourseDuration;
