import React from 'react';
import PropTypes from 'prop-types';

import './ShortInfoList.scss';

const ShortInfoList = (props) => {
	const { className, listData } = props;
	return (
		<ul className={`short-info list-reset ${className}`}>
			{listData.map((listItem, i) => (
				<li key={i} className='short-info__item'>
					{listItem.title && <b>{listItem.title}:</b>} {listItem.data}
				</li>
			))}
		</ul>
	);
};

ShortInfoList.propTypes = {
	listData: PropTypes.arrayOf(
		PropTypes.shape({
			title: PropTypes.string,
			data: PropTypes.string.isRequired,
		})
	).isRequired,
	className: PropTypes.string,
};

ShortInfoList.defaultProps = {
	className: '',
};

export default ShortInfoList;
