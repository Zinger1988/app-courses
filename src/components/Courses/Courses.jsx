import React, { useState } from 'react';
import PropTypes from 'prop-types';

import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';

import './Courses.scss';

const Courses = (props) => {
	const { coursesData, className, toggleMode } = props;
	const [foundCourses, setFoundCourses] = useState(coursesData);

	const courseCards = foundCourses.map((card) => (
		<CourseCard key={card.id} {...card} />
	));

	return (
		<div className={`courses ${className}`}>
			<div className='container'>
				<div className='courses__head'>
					<SearchBar
						className='courses__search'
						resetCards={resetCards}
						findCards={findCards}
					/>
					<Button
						className='courses__create-btn'
						style='outline-white'
						onClick={toggleMode}
						buttonText='Add new course'
					/>
				</div>
				<div className='courses__list cards-list'>
					{courseCards.length > 0 && courseCards}
					{!courseCards.length && <b>No courses by your request</b>}
				</div>
			</div>
		</div>
	);

	function findCards(searchStr) {
		const searchRes = coursesData.filter((card) => {
			const regexp = new RegExp(searchStr, 'gi');
			return card.title.match(regexp) || card.id.match(regexp);
		});
		setFoundCourses(searchRes);
	}

	function resetCards() {
		setFoundCourses(coursesData);
	}
};

Courses.propTypes = {
	coursesData: PropTypes.arrayOf(
		PropTypes.exact({
			authors: PropTypes.string,
			creationDate: PropTypes.string,
			description: PropTypes.string,
			duration: PropTypes.string,
			id: PropTypes.string,
			title: PropTypes.string,
		})
	).isRequired,
	className: PropTypes.string,
};

Courses.defaultProps = {
	className: '',
};

export default Courses;
