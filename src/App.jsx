import { useState } from 'react';

import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import CreateCourse from './components/CreateCourse/CreateCourse';

import { mockedCoursesList, mockedAuthorsList } from './constants';
import { dateGenerator } from './helpers/dateGenerator';
import { pipeDuration } from './helpers/pipeDuration';
import './App.scss';

function App() {
	const [coursesList, setCoursesList] = useState(mockedCoursesList);
	const [authorsList, setAuthorsList] = useState(mockedAuthorsList);
	const [isAddMode, setIsAddMode] = useState(false);

	const coursesData = coursesList.map((course) => {
		const courseAuthors = getAuthorsById(course.authors);
		const courseAuthorNames = courseAuthors.map((author) => author.name);
		return {
			...course,
			duration: pipeDuration(course.duration),
			creationDate: dateGenerator(),
			authors: courseAuthorNames.join(', '),
		};
	});

	return (
		<div className='app'>
			<Header className='app__header' />
			{isAddMode && (
				<CreateCourse
					coursesList={coursesList}
					setCoursesList={setCoursesList}
					authorsList={authorsList}
					setAuthorsList={setAuthorsList}
					toggleMode={toggleMode}
					getAuthorsById={getAuthorsById}
				/>
			)}
			{!isAddMode && (
				<Courses
					className='app__courses'
					toggleMode={toggleMode}
					coursesData={coursesData}
				/>
			)}
		</div>
	);

	function getAuthorsById(authorsIdList) {
		return authorsList.reduce((acc, cur) => {
			authorsIdList.includes(cur.id) && acc.push(cur);
			return acc;
		}, []);
	}

	function toggleMode() {
		setIsAddMode(!isAddMode);
	}
}

export default App;
