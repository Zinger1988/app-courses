import React from 'react';
import PropTypes from 'prop-types';
import { useFormikContext } from 'formik';

import Input from '../../../../common/Input/Input';
import Button from '../../../../common/Button/Button';

import Author from '../../../../helpers/Author';

const CreateAuthor = (props) => {
	const { authorsList, setAuthorsList } = props;
	const formikContext = useFormikContext();

	return (
		<>
			<Input
				className='course-form__input'
				placeholder='Enter author name...'
				label='Author name'
				name='newAuthor'
			/>
			<Button
				onClick={() => createAuthor(formikContext)}
				buttonText='Create author'
			/>
		</>
	);

	function createAuthor(formikProps) {
		const { setFieldValue } = formikProps;
		const { newAuthor } = formikProps.values;

		if (newAuthor.trim()) {
			setAuthorsList([...authorsList, new Author(newAuthor)]);
		}
		setFieldValue('newAuthor', '');
	}
};

CreateAuthor.propTypes = {
	authorsList: PropTypes.arrayOf(
		PropTypes.exact({
			id: PropTypes.string,
			name: PropTypes.string,
		})
	).isRequired,
	setAuthorsList: PropTypes.func.isRequired,
};

export default CreateAuthor;
