export const pipeDuration = (duration) => {
	const hours = Math.floor(duration / 60);
	const minutes = duration % 60;
	return `${getZero(hours)}:${getZero(minutes)}`;
};

function getZero(val) {
	return val < 10 ? `0${val}` : val;
}
