import React from 'react';
import PropTypes from 'prop-types';
import { Form, withFormik } from 'formik';

import Input from '../../../../common/Input/Input';
import Button from '../../../../common/Button/Button';

import './SearchBar.scss';

const SearchBar = (props) => {
	const {
		className,
		resetCards,
		setFieldValue,
		values: { search },
	} = props;

	const handleChange = () => !search && resetCards();
	const handleBlur = () => !search.trim() && setFieldValue('search', '');

	return (
		<Form className={`search-bar ${className}`}>
			<Input
				className='search-bar__input'
				placeholder='Enter course name or course ID...'
				style='white-solid'
				name='search'
				onChange={handleChange}
				onBlur={handleBlur}
			/>
			<Button
				type='submit'
				buttonText='Search'
				className='search-bar__search-btn'
				style='solid-yellow'
			/>
		</Form>
	);
};

SearchBar.propTypes = {
	className: PropTypes.string,
	findCards: PropTypes.func.isRequired,
	resetCards: PropTypes.func.isRequired,
};

SearchBar.defaultProps = {
	className: '',
};

const handleSubmit = (values, helpers) => {
	const { search } = values;
	const { findCards } = helpers.props;
	findCards(search.trim());
};

export default withFormik({
	mapPropsToValues: (props) => ({
		search: '',
	}),
	handleSubmit,
})(SearchBar);
