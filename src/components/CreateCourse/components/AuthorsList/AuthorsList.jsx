import React from 'react';
import PropTypes from 'prop-types';

import Button from '../../../../common/Button/Button';

import './AuthorsList.scss';

const AuthorsList = (props) => {
	const { list, buttonText, onClick, className } = props;

	return (
		<ul className={`authors-list list-reset ${className}`}>
			{list.map((author) => (
				<li className='authors-list__item' key={author.id}>
					<p className='authors-list__name'>{author.name}</p>
					{buttonText && (
						<Button
							className='authors-list__btn'
							size='sm'
							buttonText={buttonText}
							onClick={() => onClick(author)}
						/>
					)}
				</li>
			))}
		</ul>
	);
};

AuthorsList.propTypes = {
	list: PropTypes.array.isRequired,
	buttonText: PropTypes.string,
	onClick: PropTypes.func,
	className: PropTypes.string,
};

AuthorsList.defaultProps = {
	buttonText: '',
	onClick: () => {},
	className: '',
};

export default AuthorsList;
