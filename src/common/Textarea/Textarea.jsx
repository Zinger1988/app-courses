import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useField } from 'formik';

import ShortMessage from '../ShortMessage/ShortMessage';

import './Textarea.scss';

const Textarea = (props) => {
	const { label, className, style, name, placeholder, onChange, onBlur } =
		props;
	const [field, meta, helpers] = useField(name);
	const textareaClass = classNames(
		className,
		'textarea',
		`input--style--${style}`
	);
	const isError = meta.error && meta.touched;

	const handleChange = (e) => {
		onChange(e);
		return field.onChange(e);
	};

	return (
		<label className={textareaClass}>
			{label && <span className='textarea__label-title'>{label}</span>}
			<textarea
				{...field}
				className='textarea__control'
				placeholder={placeholder}
				name={name}
				onChange={handleChange}
				onBlur={onBlur}
			/>
			{isError && (
				<ShortMessage
					className='textarea__message'
					type='alert'
					message={meta.error}
				/>
			)}
		</label>
	);
};

Textarea.propTypes = {
	placeholder: PropTypes.string,
	onChange: PropTypes.func,
	onBlur: PropTypes.func,
	className: PropTypes.string,
	style: PropTypes.string,
	label: PropTypes.string,
	name: PropTypes.string.isRequired,
};

Textarea.defaultProps = {
	placeholder: '',
	className: '',
	style: 'black-outline',
	label: '',
	onChange: () => {},
	onBlur: () => {},
};

export default Textarea;
